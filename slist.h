#ifndef SLIST_H
#define SLIST_H

#define BLOC_STRING_ALLOC 2048

typedef struct snode_s
{
    struct snode_s *next;
    struct snode_s *prev;
    char *value;
    size_t len;
    size_t max;
} *snode_t;

typedef struct slist_s
{
    snode_t first;
    snode_t last;
    size_t len;
} *slist_t;

enum {
    FROM_BEGIN,
    FROM_END
};

void slist_add_node(slist_t l);

void slist_free(slist_t l);

void slist_append_char(slist_t l, char c);

void slist_append_int(slist_t l, int i);

snode_t slist_get(slist_t l, int from, int nb);

slist_t slist_create();

#endif /* !SLIST_H */
