#include <stdlib.h>
#include <string.h>
#include <ncurses.h>

#include "slist.h"
#include "bf.h"
#include "cui.h"

static WINDOW *mw = NULL;
static win_t sw = NULL; // stack window
static win_t pw = NULL; // program window
static win_t ow = NULL; // output window

static int oldcols;
static int oldlines;

static slist_t buffer;

/*
** Create a subwindow and add borders to it
*/
static win_t cui_new_subwin(WINDOW *p, int nl, int nc, int y, int x)
{
	win_t w = malloc(sizeof(struct win_s));
	w->win = subwin(p, nl, nc, y, x);
	w->width = nc;
	w->height = nl;
	w->x = x;
	w->y = y;
	w->posx = 1;
	w->posy = 1;
	w->dorefresh = 1;

	return w;
}

/*
** Delete the specified window
*/
static void cui_del_win(win_t w)
{
	delwin(w->win);
	free(w);
}

/*
** Print a centered title to the window
*/
static void cui_win_set_title(win_t w, const char *title)
{
	int len = strlen(title) + 2;
	int begin = w->width / 2 - len / 2;
	mvwaddch(w->win, 0, begin, ACS_RTEE);
	mvwaddstr(w->win, 0, begin + 1, title);
	mvwaddch(w->win, 0, begin + len - 1, ACS_LTEE);
}

static void cui_resize_win(win_t w, int nl, int nc, int y, int x)
{
    if(!w)
        return;

    wclear(w->win);
    mvwin(w->win, y, x);
    wresize(w->win, nl, nc);
    redrawwin(w->win);

	w->width = nc;
	w->height = nl;
	w->x = x;
	w->y = y;
	w->dorefresh = 1;

    wborder(w->win, ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE,
            ACS_ULCORNER, ACS_URCORNER, ACS_LLCORNER, ACS_LRCORNER);
}

/*
** Function to call on resize
*/
static void cui_resize()
{
    clear();
    wclear(ow->win);
    wclear(pw->win);
    wclear(sw->win);

	cui_resize_win(ow, 5, COLS, LINES-5, 0);
	cui_win_set_title(ow, "Output");

	cui_resize_win(pw, LINES-5, COLS-10, 0, 10);
	cui_win_set_title(pw, "Program");

	cui_resize_win(sw, LINES-5, 9, 0, 0);
	cui_win_set_title(sw, "Stack");

    refresh();

    /*touchwin(mw);*/

    wrefresh(sw->win);
    wrefresh(pw->win);
    wrefresh(ow->win);

    /*untouchwin(mw);*/

    oldcols = COLS;
    oldlines = LINES;
}

/*
** Init the console interface
*/
void cui_init()
{
	mw = initscr();
	cbreak();
	noecho();

	sw = cui_new_subwin(mw, LINES-5, 9, 0, 0);
	pw = cui_new_subwin(mw, LINES-5, COLS-10, 0, 10);
	ow = cui_new_subwin(mw, 5, COLS, LINES-5, 0);

    cui_resize();

    buffer = slist_create();
    slist_add_node(buffer);
}

/*
** Exit the console interface
*/
void cui_exit()
{
	while(getch() != 'q');

    slist_free(buffer);

	cui_del_win(sw);
	cui_del_win(pw);
	cui_del_win(ow);

	echo();
	nocbreak();
	endwin();
}

/*
** Set the stack window for a refresh
*/
void cui_stack_set_refresh()
{
	sw->dorefresh = 1;
}

/*
** Draw the stack
*/
static void cui_draw_stack(stack_t s)
{
	stack_node_t tmp = s->top;
	int i, j;

    int len = sw->height - 2;
    if(len > s->len)
        len = s->len;

	for(j = 1; j < sw->height - 1; j++)
		for(i = 1; i < sw->width - 1; i++)
			mvwaddch(sw->win, j, i, ' ');

	for(j = sw->height - len - 1; j < sw->height - 1; j++)
	{
		if(!tmp)
            break;
		mvwprintw(sw->win, j, 1, "%d", tmp->value);
		tmp = tmp->next;
	}

	wrefresh(sw->win);
	sw->dorefresh = 0;
}

/*
** draw the output window
** FIXME: scroll
*/
static void cui_draw_output()
{
    int i, j = 1;
    snode_t n = slist_get(buffer, FROM_END, ow->height - 2);

    while(n)
    {
        for(i = 1; i < ow->width - 1 && n->value[i-1] != '\0'; i++)
            mvwaddch(ow->win, j, i, n->value[i-1]);
        for(; i < ow->width - 1; i++)
            mvwaddch(ow->win, j, i, ' ');
        n = n->next;
        j++;
    }

	wrefresh(ow->win);
	ow->dorefresh = 0;
}

/*
** Draw the program window
*/
static void cui_draw_program(befunge_t bf)
{
	int32_t i, j;

	int halfwx = pw->width / 2;
	int halfwy = pw->height / 2;

	int halfpx = bf->width / 2;
	int halfpy = bf->height / 2;

	//int beginx = pw->width / 2 - bf->width / 2;
	//int beginy = pw->height / 2 - bf->height / 2;

	int cursx = halfwx;
	int cursy = halfwy;

	int offx = 0;
	int offy = 0;

	if(bf->ip.x < halfwx)
	{
		cursx = bf->ip.x + 1;
		offx = -1;
	}
	else if(bf->ip.x > bf->width - halfwx)
	{
		cursx = bf->ip.x - bf->width + pw->width - 1;
		offx = bf->width - pw->width + 1;
	}
	else
	{
		cursx = halfwx;
		offx = bf->ip.x - halfwx;
	}

	if(bf->ip.y < halfwy)
	{
		cursy = bf->ip.y + 1;
		offy = -1;
	}
	else if(bf->ip.y > bf->height - halfwy)
	{
		cursy = bf->ip.y - bf->height + pw->height - 1;
		offy = bf->height - pw->height + 1;
	}
	else
	{
		cursy = halfwy;
		offy = bf->ip.y - halfwy;
	}

	for(i = 1; i < pw->width-1; i++)
		for(j = 1; j < pw->height-1; j++)
			mvwaddch(pw->win, j, i, ' ');

	for(i = 0; i < bf->width; i++)
		if(i-offx > 0 && i-offx < pw->width - 1)
			for(j = 0; j < bf->height; j++)
				if(j-offy > 0 && j-offy < pw->height - 1)
					mvwaddch(pw->win, j-offy, i-offx, bf->prg[i][j]);

	wmove(pw->win, cursy, cursx);

	/*for(i = 0; i < bf->width; i++)
	{
		if(i-offx > 0 && i-offx < pw->width - 1)
		{
			for(j = 0; j < bf->height; j++)
			{
				if(j-offy > 0 && j-offy < pw->height - 1)
					mvwaddch(pw->win, j-offy, i-offx, bf->prg[i][j]);
			}
		}
	}*/

	//wmove(pw->win, cursy, cursx);

	wrefresh(pw->win);
}

/*
** Refresh the window and delay
*/
int cui_refresh(befunge_t bf)
{
	int c;

    if(COLS != oldcols || LINES != oldlines)
        cui_resize();

	if(sw->dorefresh)
		cui_draw_stack(bf->stack);
	if(ow->dorefresh)
		cui_draw_output();

	cui_draw_program(bf);

	halfdelay(5);
	if((c = getch()) != ERR)
		if(c == 'q')
			return -1;

	return 0;
}

void cui_print_int(int32_t val)
{
    slist_append_int(buffer, val);
    ow->dorefresh = 1;
}

void cui_print_char(int32_t val)
{
    slist_append_char(buffer, val);
    ow->dorefresh = 1;
}

