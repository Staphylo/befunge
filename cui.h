#ifndef CUI_H
#define CUI_H

#include <stdint.h>
#include <ncurses.h>
#include "bf.h"

#define BUFFER_ALLOC_LEN 4096

typedef struct win_s
{
	WINDOW *win;
	int width;
	int height;
	int x;
	int y;
	int posx;
	int posy;
	int dorefresh;
} *win_t;

typedef struct outwin_s
{
    struct win_s;
    int lol;
} *outwin_t;

void cui_init();
void cui_exit();
int  cui_refresh(befunge_t bf);
void cui_stack_set_refresh();
void cui_print_int(int32_t val);
void cui_print_char(int32_t val);

#endif /* !CUI_H */
