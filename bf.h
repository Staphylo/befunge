#ifndef BF_H
#define BF_H

#include <stdint.h>
#include "stack.h"

#define IP_CELL(bf) bf->prg[bf->ip.x][bf->ip.y]

enum
{
    MODE_NORMAL = 0,
    MODE_STRING = 1,
    MODE_ERROR  = 2,
    MODE_END    = 3
};

typedef enum pc_e
{
    RIGHT = 0,
    LEFT  = 1,
    UP    = 2,
    DOWN  = 3
} pc_e;

typedef struct ip_s {
    int32_t x;
    int32_t y;
} ip_s;

typedef struct befunge_s
{
    int32_t width;
    int32_t height;
    ip_s ip;
    pc_e pc;
    stack_t stack;
    int mode;
    uint8_t **prg;
} *befunge_t;


void befunge_init(befunge_t *bf);

void befunge_exit(befunge_t bf);

void befunge_parse_file(befunge_t bf, const char *filename);

void befunge_interpret(befunge_t bf);

#endif /* !BF_H */
