#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include "stack.h"
#include "cui.h"
#include "bf.h"


/*
** init the befunge interpreter
*/
void befunge_init(befunge_t *bf)
{
    srand(time(NULL));
    *bf = calloc(1, sizeof(struct befunge_s));
    (*bf)->stack = stack_empty();
    (*bf)->ip.x--;
}

/*
** free the memory required by the befunge interpreter
*/
void befunge_exit(befunge_t bf)
{
    int i;

    stack_free(bf->stack);

    for(i = 0; i < bf->width; i++)
        free(bf->prg[i]);
    free(bf->prg);

    free(bf);
}

/*
** first get the width and the height of the file
** then fill an array with these information
*/
void befunge_parse_file(befunge_t bf, const char *filename)
{
    int c;
    int32_t i = 0, j = 0;
    FILE *f = fopen(filename, "r");

    bf->width = 0;
    bf->height = 0;

    while((c = fgetc(f)) != EOF)
    {
        if(c == '\n')
        {
            if(i > bf->width)
                bf->width = i;
            bf->height++;
            i = 0;
        }
        else
            i++;
    }

    fseek(f, 0, SEEK_SET);

    bf->prg = malloc(bf->width * sizeof(uint8_t *));
    for(i = 0; i < bf->width; i++)
        bf->prg[i] = malloc(bf->height * sizeof(uint8_t));

    for(j = 0; j < bf->height; j++)
    {
        i = 0;
        while((c = fgetc(f)) != '\n' && c != EOF)
        {
            bf->prg[i][j] = c;
            i++;
        }
        for(; i < bf->width; i++)
            bf->prg[i][j] = ' ';
    }

    fclose(f);
}

/*
** move the instruction pointer
*/
static void befunge_move_ip(befunge_t bf)
{
    switch(bf->pc)
    {
        case UP:
            bf->ip.y--;
            break;
        case DOWN:
            bf->ip.y++;
            break;
        case LEFT:
            bf->ip.x--;
            break;
        case RIGHT:
            bf->ip.x++;
            break;
    }

    if(bf->ip.y < 0)
        bf->ip.y = bf->height - 1;
    else if(bf->ip.y > bf->height - 1)
        bf->ip.y = 0;
    else if(bf->ip.x < 0)
        bf->ip.x = bf->width - 1;
    else if(bf->ip.x > bf->width - 1)
        bf->ip.x = 0;
}

/*
** move the ip and then interpret the current cell with the given environment
**
*/
void befunge_interpret(befunge_t bf)
{
    int32_t tmp;

    befunge_move_ip(bf);

    if(bf->mode == MODE_STRING)
    {
        if(IP_CELL(bf) == '"')
            bf->mode = MODE_NORMAL;
        else
        {
            stack_push(bf->stack, IP_CELL(bf));
            cui_stack_set_refresh();
        }
    }
    else
    {
        switch(IP_CELL(bf))
        {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                stack_push(bf->stack, IP_CELL(bf) - '0');
                cui_stack_set_refresh();
                break;

            case ' ':
                break;

            // operator
            case '+':
                stack_push(bf->stack,
                    stack_pop(bf->stack) + stack_pop(bf->stack));
                cui_stack_set_refresh();
                break;
            case '-':
                tmp = stack_pop(bf->stack);
                stack_push(bf->stack, stack_pop(bf->stack) - tmp);
                cui_stack_set_refresh();
                break;
            case '*':
                stack_push(bf->stack,
                    stack_pop(bf->stack) * stack_pop(bf->stack));
                cui_stack_set_refresh();
                break;
            case '/':
                tmp = stack_pop(bf->stack);
                // FIXME
                if(!tmp)
                    tmp = 1;
                stack_push(bf->stack, stack_pop(bf->stack) / tmp);
                cui_stack_set_refresh();
                break;
            case '%':
                tmp = stack_pop(bf->stack);
                // FIXME
                if(!tmp)
                    tmp = 1;
                stack_push(bf->stack, stack_pop(bf->stack) % tmp);
                cui_stack_set_refresh();
                break;
            case '!':
                stack_push(bf->stack, (stack_pop(bf->stack)) ? 0 : 1);
                cui_stack_set_refresh();
                break;
            case '`':
                tmp = stack_pop(bf->stack);
                stack_push(bf->stack, (stack_pop(bf->stack) > tmp) ? 1 : 0);
                cui_stack_set_refresh();
                break;

            // move
            case '<':
                bf->pc = LEFT;
                break;
            case '>':
                bf->pc = RIGHT;
                break;
            case '^':
                bf->pc = UP;
                break;
            case 'v':
                bf->pc = DOWN;
                break;
            case '?':
                bf->pc = rand() % 4;
                break;
            case '_':
                if(stack_pop(bf->stack) == 0)
                    bf->pc = RIGHT;
                else
                    bf->pc = LEFT;
                cui_stack_set_refresh();
                break;
            case '|':
                if(stack_pop(bf->stack) == 0)
                    bf->pc = DOWN;
                else
                    bf->pc = UP;
                cui_stack_set_refresh();
                break;

            // others
            case '"':
                bf->mode = MODE_STRING;
                break;
            case ':':
                stack_push(bf->stack, stack_top(bf->stack));
                cui_stack_set_refresh();
                break;
            case '\\':
                stack_swap(bf->stack);
                cui_stack_set_refresh();
                break;
            case '$':
                stack_pop(bf->stack);
                cui_stack_set_refresh();
                break;
            case '.':
                cui_print_int(stack_pop(bf->stack));
                cui_stack_set_refresh();
                break;
            case ',':
                cui_print_char(stack_pop(bf->stack));
                cui_stack_set_refresh();
                break;
            case '#':
                befunge_move_ip(bf);
                break;
            case 'p':
                tmp = stack_pop(bf->stack);
                bf->prg[stack_pop(bf->stack)][tmp] = stack_pop(bf->stack);
                cui_stack_set_refresh();
                break;
            case 'g':
                tmp = stack_pop(bf->stack);
                stack_push(bf->stack, bf->prg[stack_pop(bf->stack)][tmp]);
                cui_stack_set_refresh();
                break;
            case '&':
                // FIXME
                break;
            case '~':
                // FIXME
                break;

            // end
            case '@':
                bf->mode = MODE_END;
                break;

            default:
                bf->mode = MODE_ERROR;
        }
    }
}
