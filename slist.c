#include <stdlib.h>
#include <string.h>

#include "slist.h"

void slist_add_node(slist_t l)
{
    snode_t n = malloc(sizeof(struct snode_s));
    n->next = NULL;
    n->prev = l->last;

    n->max = BLOC_STRING_ALLOC;
    n->value = malloc(n->max + 1);
    n->value[0] = '\0';
    n->len = 0;

    if(l->last)
        l->last->next = n;
    if(!l->first)
        l->first = n;

    l->last = n;
    l->len++;
}

void slist_free(slist_t l)
{
    snode_t n;

    if(!l)
        return;

    while(l->first)
    {
        n = l->first;
        l->first = l->first->next;
        free(n->value);
        free(n);
    }

    free(l);
}

slist_t slist_create()
{
    return calloc(1, sizeof(struct slist_s));
}

void slist_append_char(slist_t l, char c)
{
    snode_t n;
    if(l->len == 0)
        slist_add_node(l);
    n = l->last;
    if(c == '\n')
    {
        n->max = n->len + 1;
        n->value = realloc(n->value, n->max);
        slist_add_node(l);
    }
    else
    {
        if(n->len == n->max)
        {
            n->max += BLOC_STRING_ALLOC;
            n->value = realloc(n->value, n->max);
        }

        n->value[n->len] = c;
        n->value[n->len + 1] = '\0';
        n->len++;
    }
}

void slist_append_int(slist_t l, int i)
{
    int start = 1;
    while(i / start != 0)
        start *= 10;
    while(start != 1)
    {
        i = i%start;
        slist_append_char(l, i + '0');
        start /= 10;
    }
}

snode_t slist_get(slist_t l, int from, int nb)
{
    snode_t n;

    if(from == FROM_END)
    {
        n = l->last;
        while(n->prev && --nb)
            n = n->prev;
    }
    else
    {
        n = l->first;
        while(n->next && --nb)
            n = n->next;
    }

    return n;
}
