#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <time.h>

#include "cui.h"
#include "bf.h"

void loop(befunge_t bf)
{
    while(bf->mode != MODE_END)
    {
        if(bf->mode == MODE_END)
        {
            // display error

            break;
        }

        befunge_interpret(bf);

        if(cui_refresh(bf) == -1)
            break;
    }
}

/*
** Entry point of the program
*/
int main(int argc, char **argv)
{
    befunge_t bf;

    if(argc < 2)
    {
        fprintf(stderr, "usage: %s <file>\n", argv[0]);
        return EXIT_FAILURE;
    }

    if(access(argv[1], R_OK) != 0)
    {
        fprintf(stderr, "can't open file %s\n", argv[1]);
        return EXIT_FAILURE;
    }

    cui_init();
    befunge_init(&bf);
    befunge_parse_file(bf, argv[1]);
    loop(bf);
    befunge_exit(bf);
    cui_exit();

    return EXIT_SUCCESS;
}
