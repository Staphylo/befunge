#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include "cui.h"
#include "stack.h"

stack_t stack_empty()
{
    return calloc(1, sizeof(struct stack_s));
}

void stack_push(stack_t s, int32_t val)
{
    stack_node_t n = malloc(sizeof(struct stack_node_s));
    n->value = val;
    n->next = s->top;
    s->top = n;
    s->len++;
}

int32_t stack_pop(stack_t s)
{
    if(s->len == 0)
        return 0;

    stack_node_t o = s->top;
    int32_t val = o->value;
    s->top = s->top->next;
    s->len--;

    free(o);

    return val;
}

int32_t stack_top(stack_t s)
{
    if(s->len == 0)
        return 0;

    return s->top->value;
}

void stack_swap(stack_t s)
{
    int32_t a = stack_pop(s);
    int32_t b = stack_pop(s);

    stack_push(s, a);
    stack_push(s, b);
}

void stack_free(stack_t s)
{
    stack_node_t o;
    while(s->top)
    {
        o = s->top;
        s->top = s->top->next;
        free(o);
    }

    free(s);
}
