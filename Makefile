CC=clang
CFLAGS=-Wall -Wextra -g
LDFLAGS=-lncurses
SRC=slist.c stack.c bf.c cui.c main.c
OBJ=$(SRC:.c=.o)
EXEC=befunge

all: $(EXEC)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(EXEC): $(OBJ)
	$(CC) $(LDFLAGS) $(CFLAGS) $(OBJ) -o $(EXEC)

clean::
	rm -f *.o *~ $(EXEC)

