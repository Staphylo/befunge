#ifndef STACK_H
#define STACK_H

#include <stdint.h>

typedef struct stack_node_s
{
    int32_t value;
    struct stack_node_s *next;
} * stack_node_t;

typedef struct stack_s
{
    int32_t len;
    stack_node_t top;
} * stack_t;


stack_t stack_empty();

void stack_push(stack_t s, int32_t val);

int32_t stack_pop(stack_t s);

int32_t stack_top(stack_t s);

void stack_swap(stack_t s);

void stack_free(stack_t s);

#endif /* !STACK_H */
